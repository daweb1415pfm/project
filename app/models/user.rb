class User < ActiveRecord::Base
  validates :name, :birthday, :locality, presence: true
  belongs_to :login
  has_many :activities, dependent: :destroy
  has_many :sports, :through => :activities
  has_many :maps, :through => :activities
  has_many :friendships, dependent: :destroy
  has_many :friends, :through => :friendships
end
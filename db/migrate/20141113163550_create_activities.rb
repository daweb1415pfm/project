class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name
      t.integer :sport_id
      t.integer :user_id
      t.integer :map_id
      t.datetime :start_date
      t.integer :duration
      t.integer :distance

      t.timestamps
    end
  end
end

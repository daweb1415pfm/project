class RemoveLoginIdFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :id_login, :integer
  end
end
